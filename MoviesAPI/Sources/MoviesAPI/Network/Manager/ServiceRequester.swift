//
//  ServiceRequester.swift
//  Movies
//
//  Created by Luis Manuel Castillo Zamorano on 5/12/21.
//  Copyright © 2021 luiscastillo. All rights reserved.
//

import Foundation

public final class ServiceRequester {
    
    public class func request<T: Decodable>(router: URLRequestConvertible,
                                     completion: @escaping (Result<T, NetworkError>) -> Void) {
        self.request(router: router) { result in
            completion(result.flatMap({ data in
                do {
                    let decoder = JSONDecoder()
                    decoder.keyDecodingStrategy = .convertFromSnakeCase
                    let responseObject = try decoder.decode(T.self, from: data)
                    return .success(responseObject)
                } catch {
                    return .failure(.invalidJSON(error))
                }
            }))
        }
    }
    
    public class func request(router: URLRequestConvertible, session: URLSession = .shared,
                       completion: @escaping (Result<Data, NetworkError>) -> Void) {
        var components = URLComponents()
        components.scheme = router.scheme
        components.host = router.host
        components.path = router.path
        components.queryItems = router.parameters
        
        guard let url = components.url else { return }
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = router.method.rawValue
        
        let dataTask = session.dataTask(with: urlRequest) { data, response, error in
            DispatchQueue.main.async {
                if let responseError = error {
                    completion(.failure(.serverError))
                    print(responseError.localizedDescription)
                    return
                }
                
                guard let httpResponse = response as? HTTPURLResponse else {
                    return completion(.failure(.noData))
                }
                
                guard 200..<300 ~= httpResponse.statusCode else {
                    return completion(.failure(.invalidStatusCode(httpResponse.statusCode)))
                }
                
                guard let data = data else {
                    return completion(.failure(.noData))
                }
                
                completion(.success(data))
            }
        }
        dataTask.resume()
    }
    
    
}
