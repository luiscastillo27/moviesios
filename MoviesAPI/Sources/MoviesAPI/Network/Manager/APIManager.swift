//
//  APIManager.swift
//  Movies
//
//  Created by Luis Manuel Castillo Zamorano on 5/12/21.
//  Copyright © 2021 luiscastillo. All rights reserved.
//

import Foundation

public enum MovieListState {
    case feed
    case filtered(query: String)
    case favorite
    case watchList
}

public struct APIManager {
    public static let urlBase: String = "api.themoviedb.org"
    public static let imageUrlBase: String = "https://image.tmdb.org/t/p/w500"
}

public enum HTTPHeaderField: String {
    case authentication = "Authorization"
}

public enum HTTPMethod: String {
    case get = "GET"
    case post = "POST"
}

public protocol URLRequestConvertible {
    var scheme: String { get }
    var host: String { get }
    var path: String { get }
    var parameters: [URLQueryItem] { get }
    var method: HTTPMethod { get }
}
