//
//  MovieServiceCallerProtocol.swift
//  Movies
//
//  Created by Luis Manuel Castillo Zamorano on 5/12/21.
//  Copyright © 2021 luiscastillo. All rights reserved.
//

import Foundation

public protocol MovieServiceCallerProtocol {
    func getMovies(state: MovieListState, completion: @escaping(_ result: Result<MovieListResponse, NetworkError>) -> Void)
    func markAsFavorite(movie: Movie, isCurrentFavorite: Bool, completion: @escaping (Result<StatusResponse, NetworkError>) -> Void)
    func addToWatchList(movie: Movie, isCurrentOnWatchList: Bool, completion: @escaping (Result<StatusResponse, NetworkError>) -> Void)
}
