//
//  MoveServiceCaller.swift
//  Movies
//
//  Created by Luis Manuel Castillo Zamorano on 5/12/21.
//  Copyright © 2021 luiscastillo. All rights reserved.
//

import Foundation

public struct MovieServiceCaller: MovieServiceCallerProtocol {
    public init(){}
    public func getMovies(state: MovieListState, completion: @escaping (Result<MovieListResponse, NetworkError>) -> Void) {
        let router = MovieRouter.getMovies(state: state)        
        ServiceRequester.request(router: router) { (result: Result<MovieListResponse, NetworkError>) in
            completion(result)
        }
    }
    
    public func markAsFavorite(movie: Movie, isCurrentFavorite: Bool, completion: @escaping (Result<StatusResponse, NetworkError>) -> Void) {
        let router = MovieRouter.markAsFavorite(movie: movie, isCurrentFavorite: isCurrentFavorite)
        ServiceRequester.request(router: router) { (result: Result<StatusResponse, NetworkError>) in
            completion(result)
        }
    }
    
    public func addToWatchList(movie: Movie, isCurrentOnWatchList: Bool, completion: @escaping (Result<StatusResponse, NetworkError>) -> Void) {
        let router = MovieRouter.addToWatchList(movie: movie, isCurrentOnWatchList: isCurrentOnWatchList)
        ServiceRequester.request(router: router) { (result: Result<StatusResponse, NetworkError>) in
            completion(result)
        }
    }
}
