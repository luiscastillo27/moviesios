//
//  MoveRouter.swift
//  Movies
//
//  Created by Luis Manuel Castillo Zamorano on 5/12/21.
//  Copyright © 2021 luiscastillo. All rights reserved.
//

import Foundation

public enum MovieRouter: URLRequestConvertible {
    
    case getMovies(state: MovieListState)
    case markAsFavorite(movie: Movie, isCurrentFavorite: Bool)
    case addToWatchList(movie: Movie, isCurrentOnWatchList: Bool)
    
    public var method: HTTPMethod {
        switch self {
        case .getMovies:
            return .get
        case .markAsFavorite, .addToWatchList:
            return .post
        }
    }
    
    public var scheme: String {
        return "https"
    }
    
    public  var host: String {
        return APIManager.urlBase
    }
    
    public var path: String {
        switch self {
        case .getMovies(let state):
            switch state {
            case .feed:
                return "/3/movie/popular"
            case .filtered:
                return "/3/search/movie"
            case .favorite:
                return "/3/account/\(SecretKey.idAccount)/favorite/movies"
            case .watchList:
                return "/3/account/\(SecretKey.idAccount)/watchlist/movies"
            }
            
        case .markAsFavorite:
            return "/3/account/\(SecretKey.idAccount)/favorite"
            
        case .addToWatchList:
            return "/3/account/\(SecretKey.idAccount)/watchlist"
        }
    }
    
    public var parameters: [URLQueryItem] {
        switch self {
        case .getMovies(let state):
            switch state {
                
            case .feed:
                let currentDate = Calendar.current.component(.year, from: Date())
                return [
                    URLQueryItem(name: "api_key", value: SecretKey.apiKey),
                    URLQueryItem(name: "language", value: "en-US"),
                    URLQueryItem(name: "primary_release_year", value: "\(currentDate)")
                ]
            case .filtered(let query):
                return [
                    URLQueryItem(name: "api_key", value: SecretKey.apiKey),
                    URLQueryItem(name: "query", value: query),
                    URLQueryItem(name: "language", value: "en-US")
                ]
            case .favorite, .watchList:
                return [
                    URLQueryItem(name: "api_key", value: SecretKey.apiKey),
                    URLQueryItem(name: "session_id", value: SecretKey.idSession),
                    URLQueryItem(name: "sort_by", value: "created_at.desc"),
                    URLQueryItem(name: "language", value: "en-US")
                ]
            }
            
        case .markAsFavorite(let movie, let isCurrentFavorite):
            return [
                URLQueryItem(name: "api_key", value: SecretKey.apiKey),
                URLQueryItem(name: "session_id", value: SecretKey.idSession),
                URLQueryItem(name: "media_type", value: "movie"),
                URLQueryItem(name: "media_id", value: "\(movie.id)"),
                URLQueryItem(name: "favorite", value: "\(!isCurrentFavorite)")
            ]
        case .addToWatchList(movie: let movie, isCurrentOnWatchList: let isCurrentOnWatchList):
            return [
                URLQueryItem(name: "api_key", value: SecretKey.apiKey),
                URLQueryItem(name: "session_id", value: SecretKey.idSession),
                URLQueryItem(name: "media_type", value: "movie"),
                URLQueryItem(name: "media_id", value: "\(movie.id)"),
                URLQueryItem(name: "favorite", value: "\(!isCurrentOnWatchList)")
            ]
        }
    }
    

    
}
