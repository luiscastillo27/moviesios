//
//  Movie.swift
//  Movies
//
//  Created by Luis Manuel Castillo Zamorano on 5/12/21.
//  Copyright © 2021 luiscastillo. All rights reserved.
//

import Foundation

public struct Movie: Decodable {
    public let id: Int
    public let title: String
    public let voteAverage: Double
    public let posterPath: String
    public let overview: String
    public let releaseDate: String
}
