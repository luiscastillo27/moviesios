//
//  MovieListResponse.swift
//  Movies
//
//  Created by Luis Manuel Castillo Zamorano on 5/12/21.
//  Copyright © 2021 luiscastillo. All rights reserved.
//

import Foundation

public struct MovieListResponse: Decodable {
    public var movies: [Movie]
    
    enum CodingKeys: String, CodingKey {
        case movies = "results"
    }
}
