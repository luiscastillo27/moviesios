//
//  StatusResponse.swift
//  Movies
//
//  Created by Luis Manuel Castillo Zamorano on 5/12/21.
//  Copyright © 2021 luiscastillo. All rights reserved.
//

import Foundation

public struct StatusResponse: Decodable {
    public let statusCode: Int
    public let statusMessage: String
}
