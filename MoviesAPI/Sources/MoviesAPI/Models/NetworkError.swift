//
//  NetworkError.swift
//  Movies
//
//  Created by Luis Manuel Castillo Zamorano on 5/12/21.
//  Copyright © 2021 luiscastillo. All rights reserved.
//

import Foundation

public enum NetworkError: Error {
    case serverError
    case invalidJSON(Error)
    case invalidStatusCode(Int)
    case noData
    case unknown(Error)
    
    public var localizedDescription: String {
        switch self {
        case .serverError:
            return "Error on the Server"
        case .invalidJSON(let error):
            return "The JSON is not valid: \(error.localizedDescription)"
        case .invalidStatusCode(let code):
            return "Invalid Status Code: \(code)"
        case .noData:
            return "Data Not Found"
        case .unknown(let error):
            return error.localizedDescription
        }
    }
}
