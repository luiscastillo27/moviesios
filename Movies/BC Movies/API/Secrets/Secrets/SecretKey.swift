struct SecretKey {
    static let apiKey: String = "c72f9342873cce8be22f878b9d5925f1" //Get from https://developers.themoviedb.org/3/getting-started/introduction
    static let accesstoken: String = "eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJjNzJmOTM0Mjg3M2NjZThiZTIyZjg3OGI5ZDU5MjVmMSIsInN1YiI6IjYxYWM1NjBhMGQyZjUzMDA0MTA2ZmRkNCIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.EXdf2sE-Zk927Ukv3wrLCGBb8P9sSBLEXder1uMNBSw" //Get from https://developers.themoviedb.org/3/getting-started/introduction
    static let idAccount: Int = 0 //Get from https://developers.themoviedb.org/3/account/get-account-details
    static let idSession: String = "" // Get from https://developers.themoviedb.org/3/authentication/how-do-i-generate-a-session-id
}