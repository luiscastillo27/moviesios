//
//  DownloadImage+Ext.swift.swift
//  BC Movies
//
//  Created by ITS Areomexico on 11/12/21.
//  Copyright © 2021 saulurias. All rights reserved.
//

import UIKit

extension UIImageView {
    
    func downloadImage(imageURL:String) {
        guard let url = URL(string: imageURL) else { return }
        URLSession.shared.downloadTask(with: url) { fileURL, response, error in
            if let error = error {
                print("The error is: ", error.localizedDescription)
            } else {
                guard let imageURL = fileURL else { return }
                guard let data = try? Data(contentsOf: imageURL) else { return }
                let image = UIImage(data: data)
                DispatchQueue.main.async {
                    self.image = image
                }
                print("We want to display the image: ", fileURL ?? "")
            }
        }.resume()
    }
    
}
