//
//  NiblessView.swift
//  Movies
//
//  Created by Luis Manuel Castillo Zamorano on 5/12/21.
//  Copyright © 2021 luiscastillo. All rights reserved.
//

import UIKit

class NiblessView: UIView {
    public init() {
        super.init(frame: .zero)
    }
    
    @available(*,
    unavailable,
    message: "Loading this view controller from a nib is unsupported in favor of initializer dependency injection."
    )
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
