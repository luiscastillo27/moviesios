//
//  NiblessCollectionViewController.swift
//  Movies
//
//  Created by Luis Manuel Castillo Zamorano on 5/12/21.
//  Copyright © 2021 luiscastillo. All rights reserved.
//

import UIKit

class NiblessCollectionViewController: UICollectionViewController {
    public init(layout: UICollectionViewLayout) {
        super.init(collectionViewLayout: layout)
    }
    
    @available(*,
    unavailable,
    message: "Loading this view controller from a nib is unsupported in favor of initializer dependency injection." )
    public override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    @available(*,
    unavailable,
    message: "Loading this view controller from a nib is unsupported in favor of initializer dependency injection."
    )
    public required init?(coder aDecoder: NSCoder) {
        fatalError("This view is not compatible to load from a nib/storyboard")
    }
}
