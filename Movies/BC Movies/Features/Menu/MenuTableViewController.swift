//
//  MenuTableViewController.swift
//  Movies
//
//  Created by Luis Manuel Castillo Zamorano on 5/12/21.
//  Copyright © 2021 luiscastillo. All rights reserved.
//

import UIKit

protocol MenuDelegate: AnyObject {
    func menuTableViewController(_ tableView: MenuTableViewController, selected option: String)
}

final class MenuTableViewController: UITableViewController {
    // MARK: Constants and Variables
    private lazy var cancelButton = UIBarButtonItem(title: "Cancel",
                                                    style: .plain,
                                                    target: self,
                                                    action: #selector(cancelPressed))
    private let menuOptions = ["Watch List"]
    weak var delegate: MenuDelegate?
    
    // MARK: View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpNavigationBar()
    }
    
    private func setUpNavigationBar() {
        title = "Menu"
        navigationItem.leftBarButtonItem = cancelButton
    }
    
    @objc private func cancelPressed() {
        dismiss(animated: true)
    }

    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuOptions.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = menuOptions[indexPath.row]
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        dismiss(animated: true) {
            self.delegate?.menuTableViewController(self, selected: self.menuOptions[indexPath.row])
        }
    }
}
