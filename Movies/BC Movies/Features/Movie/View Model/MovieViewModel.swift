//
//  MovieViewModel.swift
//  Movies
//
//  Created by Luis Manuel Castillo Zamorano on 5/12/21.
//  Copyright © 2021 luiscastillo. All rights reserved.
//

import Foundation
import UIKit.UIImageView
import MoviesAPI

// MARK: Movie List States

final class MovieViewModel {
    // MARK: Constants and Variables
    let serviceCaller: MovieServiceCallerProtocol
    private var movies: [Movie] = []
    private var filteredMovies: [Movie] = []
    private var favoriteMovies: [Movie] = []
    private var watchListMovies: [Movie] = []
    var networkError: Box<NetworkError?> = Box(nil)
    var didLoadMovies: Box<Bool?> = Box(nil)
    var addedToWatchList: Box<Bool?> = Box(nil)
    var movieListState: MovieListState = .feed
    
    // MARK: Init
    init(serviceCaller: MovieServiceCallerProtocol = MovieServiceCaller()) {
        self.serviceCaller = serviceCaller
        reloadFavoriteMovies()
    }
    
    // MARK: Functions
    func getMovie(at index: Int) -> Movie? {
        switch movieListState {
        case .feed:
            return movies[safe: index]
        case .filtered:
            return filteredMovies[safe: index]
        case .favorite:
            return favoriteMovies[safe: index]
        case .watchList:
            return watchListMovies[safe: index]
        }
    }
    
    func totalMovies() -> Int {
        switch movieListState {
        case .feed:
            return movies.count
        case .filtered:
            return filteredMovies.count
        case .favorite:
            return favoriteMovies.count
        case .watchList:
            return watchListMovies.count
        }
    }
    
    private func checkPreloadedMovies() -> Bool {
        switch movieListState {
        case .feed: return movies.isEmpty
        case .favorite: return favoriteMovies.isEmpty
        case .watchList: return watchListMovies.isEmpty
        default: return true
        }
    }
    
    func isFavorite(movie: Movie) -> Bool {
        return favoriteMovies.contains { favoriteMovie -> Bool in
            return movie.id == favoriteMovie.id
        }
    }
}

// MARK: - Service Requests
extension MovieViewModel {
    func loadMovies() {
        if checkPreloadedMovies() {
            serviceCaller.getMovies(state: movieListState) { result in
                switch result {
                case .success(let moviesResponse):
                    print("Did load movies")
                    switch self.movieListState {
                    case .feed:
                        self.movies = moviesResponse.movies
                    case .filtered:
                        self.filteredMovies = moviesResponse.movies
                    case .favorite:
                        self.favoriteMovies = moviesResponse.movies
                    case .watchList:
                        self.watchListMovies = moviesResponse.movies
                    }
                    self.didLoadMovies.value = true
                case .failure(let error):
                    self.didLoadMovies.value = false
                    self.networkError.value = error
                }
            }
        } else {
            print("Pre loaded movies")
            self.didLoadMovies.value = true
        }
    }
    
    private func reloadFavoriteMovies() {
        serviceCaller.getMovies(state: .favorite) { result in
            switch result {
            case .success(let moviesResponse):
                self.favoriteMovies = moviesResponse.movies
                self.didLoadMovies.value = true
            case .failure(let error):
                self.networkError.value = error
            }
        }
    }
    
    func markAsFavorite(_ movie: Movie) {
        serviceCaller.markAsFavorite(movie: movie, isCurrentFavorite: isFavorite(movie: movie)) { result in
            switch result {
            case .success:
                self.reloadFavoriteMovies()
            case .failure(let error):
                self.networkError.value = error
            }
        }
    }
    
    func addToWatchList(_ movie: Movie) {
        serviceCaller.addToWatchList(movie: movie, isCurrentOnWatchList: false) { result in
            switch result {
            case .success:
                self.addedToWatchList.value = true
            case .failure(let error):
                self.networkError.value = error
            }
        }
    }
}
