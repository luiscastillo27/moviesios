//
//  MovieDetailView.swift
//  Movies
//
//  Created by Luis Manuel Castillo Zamorano on 5/12/21.
//  Copyright © 2021 luiscastillo. All rights reserved.
//

import UIKit
import MoviesAPI

final class MovieDetailView: NiblessView {
    // MARK: Constants and Variables
    private enum DesignConstants {
        static let titleFontSize: CGFloat = 27
        static let titleNumberOfLines: Int = .zero
        static let titleInsets = UIEdgeInsets(top: 5, left: 2, bottom: .zero, right: -2)
        static let posterHeight: CGFloat = 200
        static let favoriteButtonInsets = UIEdgeInsets(top: 5, left: .zero, bottom: -2, right: -3)
        static let favoriteButtonSize = CGSize(width: 25, height: 25)
        static let ratingLabelWidth: CGFloat = 100
        static let ratingLabelBottomInset: CGFloat = -3
    }
    
    private let scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.backgroundColor = .white
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        return scrollView
    }()
    
    private let posterImageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "placeholder"))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.font = .boldSystemFont(ofSize: DesignConstants.titleFontSize)
        label.numberOfLines = DesignConstants.titleNumberOfLines
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let ratingLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let dateLabel: UILabel = {
        let label = UILabel()
        label.textColor = .darkGray
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let overviewTextView: UITextView = {
        let textView = UITextView()
        textView.isScrollEnabled = false
        textView.isEditable = false
        textView.font = .systemFont(ofSize: 20)
        textView.translatesAutoresizingMaskIntoConstraints = false
        return textView
    }()
    
    private let movie: Movie
    
    // MARK: View Life Cycle
    init(movie: Movie) {
        self.movie = movie
        super.init()
        setUpView()
        setUpOutlets()
    }
    
    private func setUpView() {
        backgroundColor = .white
        addSubview(scrollView)
        scrollView.addSubview(posterImageView)
        scrollView.addSubview(titleLabel)
        scrollView.addSubview(ratingLabel)
        scrollView.addSubview(dateLabel)
        scrollView.addSubview(overviewTextView)
        
        NSLayoutConstraint.activate([
            scrollView.frameLayoutGuide.topAnchor.constraint(equalTo: topAnchor),
            scrollView.frameLayoutGuide.leadingAnchor.constraint(equalTo: leadingAnchor),
            scrollView.frameLayoutGuide.trailingAnchor.constraint(equalTo: trailingAnchor),
            scrollView.frameLayoutGuide.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
        
        NSLayoutConstraint.activate([
            posterImageView.topAnchor.constraint(equalTo: scrollView.contentLayoutGuide.topAnchor),
            posterImageView.leadingAnchor.constraint(equalTo: scrollView.contentLayoutGuide.leadingAnchor),
            posterImageView.trailingAnchor.constraint(equalTo: scrollView.contentLayoutGuide.trailingAnchor),
            posterImageView.trailingAnchor.constraint(equalTo: trailingAnchor),
            posterImageView.heightAnchor.constraint(equalToConstant: 350)
        ])
        
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: posterImageView.bottomAnchor),
            titleLabel.leadingAnchor.constraint(equalTo: posterImageView.leadingAnchor,
                                                constant: DesignConstants.titleInsets.left),
            titleLabel.trailingAnchor.constraint(equalTo: posterImageView.trailingAnchor,
                                                 constant: DesignConstants.titleInsets.right),
            ratingLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor,
                                             constant: DesignConstants.ratingLabelBottomInset),
            ratingLabel.leadingAnchor.constraint(equalTo: titleLabel.leadingAnchor),
            ratingLabel.widthAnchor.constraint(equalToConstant: DesignConstants.ratingLabelWidth),
            dateLabel.topAnchor.constraint(equalTo: ratingLabel.bottomAnchor),
            dateLabel.leadingAnchor.constraint(equalTo: ratingLabel.leadingAnchor),
            overviewTextView.topAnchor.constraint(equalTo: dateLabel.bottomAnchor, constant: DesignConstants.favoriteButtonInsets.bottom),
            overviewTextView.leadingAnchor.constraint(equalTo: titleLabel.leadingAnchor),
            overviewTextView.trailingAnchor.constraint(equalTo: titleLabel.trailingAnchor),
            overviewTextView.bottomAnchor.constraint(equalTo: scrollView.contentLayoutGuide.bottomAnchor)
        ])
    }
        
    // MARK: Functions
    private func setUpOutlets() {
        titleLabel.text = movie.title
        ratingLabel.text = "Rate: \(movie.voteAverage)"
        dateLabel.text = movie.releaseDate
        overviewTextView.text = movie.overview
        posterImageView.downloadImage(imageURL: "\(APIManager.imageUrlBase)\(movie.posterPath)")
    }
}
