//
//  MovieDetailViewController.swift
//  Movies
//
//  Created by Luis Manuel Castillo Zamorano on 5/12/21.
//  Copyright © 2021 luiscastillo. All rights reserved.
//

import UIKit
import MoviesAPI

final class MovieDetailViewController: NiblessViewController {
    // MARK: Constants and Variables
    private lazy var detailView = MovieDetailView(movie: movie)
    private let movie: Movie
    private let viewModel: MovieViewModel
    
    // MARK: View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Movie Detail"
        view.backgroundColor = .white
        detailView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(detailView)
        NSLayoutConstraint.activate([
            detailView.topAnchor.constraint(equalTo: view.topAnchor),
            detailView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            detailView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            detailView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
    }
    
    init(movie: Movie, viewModel: MovieViewModel) {
        self.movie = movie
        self.viewModel = viewModel
        super.init()
    }
}
