//
//  MoviesWatchListCollectionViewController.swift
//  Movies
//
//  Created by Luis Manuel Castillo Zamorano on 5/12/21.
//  Copyright © 2021 luiscastillo. All rights reserved.
//

import UIKit

protocol MoviesWatchListDelegate: AnyObject {
    func moviesWatchListDismiss()
}

final class MoviesWatchListCollectionViewController: NiblessCollectionViewController {
    // MARK: Variables and Constants
    private enum DesignConstants {
        static let movieCollectionViewCellId = "movieCollectionViewCellIdentifier"
        static let collectionViewLayoutInsets = UIEdgeInsets(top: 15, left: 15, bottom: 15, right: 15)
        static let collectionViewMiniumLineSpacing: CGFloat = 25
        static let collectionViewCellHeight: CGFloat = 270
    }
    
    private lazy var cancelButton = UIBarButtonItem(title: "Cancel",
                                                    style: .plain,
                                                    target: self,
                                                    action: #selector(cancelPressed))
    
    let viewModel: MovieViewModel
    weak var delegate: MoviesWatchListDelegate?
    
    init(viewModel: MovieViewModel) {
        self.viewModel = viewModel
        self.viewModel.movieListState = .watchList
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = DesignConstants.collectionViewLayoutInsets
        layout.minimumLineSpacing = DesignConstants.collectionViewMiniumLineSpacing
        super.init(layout: layout)
    }
    
    // MARK: Functions View Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpCollectionView()
        setUpNavigationBar()
        setUpBindings()
        viewModel.loadMovies()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        delegate?.moviesWatchListDismiss()
    }
    
    // MARK: Functions
    private func setUpCollectionView() {
        collectionView.backgroundColor = .white
        collectionView?.register(MovieCollectionViewCell.self,
                                 forCellWithReuseIdentifier: DesignConstants.movieCollectionViewCellId)
    }
    
    private func setUpNavigationBar() {
        title = "Watch List"
        navigationItem.leftBarButtonItem = cancelButton
    }
    
    private func setUpBindings() {
        viewModel.didLoadMovies.bind { [weak self] _ in
            self?.collectionView?.reloadData()
        }
    }
    
    @objc private func cancelPressed() {
        dismiss(animated: true)
    }
}

// MARK: UICollectionViewDataSource
extension MoviesWatchListCollectionViewController {
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.totalMovies()
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: DesignConstants.movieCollectionViewCellId,
                                                            for: indexPath) as? MovieCollectionViewCell else {
            assertionFailure("Unable to parse MovieCollectionViewCell at MoviesViewController")
            return UICollectionViewCell()
        }
        
        guard let movie = viewModel.getMovie(at: indexPath.row) else { return UICollectionViewCell() }
        
        cell.configure(with: movie, isWatchList: true)
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let movie = viewModel.getMovie(at: indexPath.row) else { return }
        let detailViewController = MovieDetailViewController(movie: movie, viewModel: viewModel)
        navigationController?.pushViewController(detailViewController, animated: true)
    }
}

// MARK: UICollectionViewDelegateFlowLayout
extension MoviesWatchListCollectionViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (view.frame.width/2) - 20
        return CGSize(width: width, height: DesignConstants.collectionViewCellHeight)
    }
}
