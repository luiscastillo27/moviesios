//
//  UIViewController+Ext.swift
//  exampleMovies
//
//  Created by ITS Areomexico on 15/12/21.
//

import UIKit

extension UIViewController {
    
    func embedInNavigation() -> UINavigationController {
        let UINavigation = UINavigationController(rootViewController: self)
        return UINavigation
    }
    
    public func alertSimple( this: UIViewController, titileAlert: String?, bodyAlert: String?, complete: @escaping ((String) -> Void)) {
        let alert = UIAlertController(title: titileAlert, message: bodyAlert, preferredStyle: .alert)
        let acept = UIAlertAction(title: "Aceptar", style: .default, handler: { (action) in
            complete("acept")
        })
        alert.addAction(acept)
        this.present(alert, animated: true, completion: nil)
    }
    
    public func hendlerError( this: UIViewController, complete: @escaping ((String) -> Void)) {
        let alert = UIAlertController(title: "Se ha producido un error", message: "Inténtalo más tarde", preferredStyle: .alert)
        let acept = UIAlertAction(title: "Aceptar", style: .default, handler: { (action) in
            complete("acept")
        })
        alert.addAction(acept)
        this.present(alert, animated: true, completion: nil)
    }

    
}
