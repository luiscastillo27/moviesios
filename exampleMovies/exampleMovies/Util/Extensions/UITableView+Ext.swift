//
//  UITableView+Ext.swift
//  exampleMovies
//
//  Created by ITS Areomexico on 15/12/21.
//

import UIKit

extension UITableView {
    func configure( owner: UIViewController ){
        tableFooterView = UIView()
        separatorStyle = .none
        delegate = owner as? UITableViewDelegate
        dataSource = owner as? UITableViewDataSource
    }
}

