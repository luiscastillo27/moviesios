//
//  LaunchViewController.swift
//  exampleMovies
//
//  Created by ITS Areomexico on 15/12/21.
//

import UIKit

final class LaunchViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    private func setup(){
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            let homeController = HomeViewController().embedInNavigation()
            homeController.modalPresentationStyle = .fullScreen
            self.present(homeController, animated: false, completion: nil)
        }
    }

}
