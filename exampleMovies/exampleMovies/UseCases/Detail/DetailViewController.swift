//
//  DetailViewController.swift
//  exampleMovies
//
//  Created by ITS Areomexico on 15/12/21.
//

import UIKit

final class DetailViewController: UIViewController {

    // MARK: IBOutlets
    
    // MARK: Variables and Constants
    
    // MARK: View Lifes
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    init(){
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setup(){
        title = "Movie Detail"
    }

}
