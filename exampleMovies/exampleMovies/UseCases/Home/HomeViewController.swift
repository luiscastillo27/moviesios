//
//  HomeViewController.swift
//  exampleMovies
//
//  Created by ITS Areomexico on 15/12/21.
//

import UIKit

final class HomeViewController: UIViewController {

    // MARK: IBOutlets
    @IBOutlet weak var collectionView: UICollectionView!
    
    // MARK: Constants and Variables
    let collectionViewLayoutInsets = UIEdgeInsets(top: 15, left: 15, bottom: 15, right: 15)
    let collectionViewMiniumLineSpacing: CGFloat = 25
    let collectionViewCellHeight: CGFloat = 270
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }

    // MARK: Functions
    private func setup(){
        title = "Feed"
        let collectionViewFlow = UICollectionViewFlowLayout()
        collectionViewFlow.sectionInset = collectionViewLayoutInsets
        collectionViewFlow.minimumLineSpacing = collectionViewMiniumLineSpacing
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.collectionViewLayout = collectionViewFlow
        collectionView.register(UINib(nibName: MovieCell.kCellId, bundle: nil), forCellWithReuseIdentifier: MovieCell.kCellId)
    }

}

// MARK: CollectionDataSource
extension HomeViewController: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 27
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MovieCell.kCellId, for: indexPath) as? MovieCell {
            return cell
        }
       return UICollectionViewCell()
    }
    
}

// MARK: CollectionFlowLayout
extension HomeViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (view.frame.width/2) - 20
        return CGSize(width: width, height: collectionViewCellHeight)
    }
}

// MARK: CollectionViewDelegate
extension HomeViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("SELECT: ", indexPath.row)
        let vc = DetailViewController()
        navigationController?.pushViewController(vc, animated: true)
    }
}
