#Movie
Coding Challenge

This project is powered by [themoviedb API](https://www.themoviedb.org/documentation/api)

# IMPORTANT
To be able to run this project a Secret Folder is required and its on the ignore file for security, it will be passed by the developer on a secure way

```
struct SecretKey {
    static let apiKey: String = "" //Get from https://developers.themoviedb.org/3/getting-started/introduction
    static let accesstoken: String = "" //Get from https://developers.themoviedb.org/3/getting-started/introduction
    static let idAccount: Int = 0 //Get from https://developers.themoviedb.org/3/account/get-account-details
    static let idSession: String = "" // Get from https://developers.themoviedb.org/3/authentication/how-do-i-generate-a-session-id
}
```

## Project Making Usage Of:
- Swift 5
- SOLID and Clean Code principles
- Dependency Injection
- AutoLayout
- Repository pattern

*Some Extras*

- SwiftLint
- MVVM

